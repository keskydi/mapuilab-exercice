var Drug = require('../models/drug');

class DrugService {

    static addDrug(name,code) {
        return Drug({
            name: name,
            code: code
        }).save();
    }

    static deleteDrug(id) {
        return Drug.findByIdAndDelete(id);
    }

}

module.exports = DrugService;