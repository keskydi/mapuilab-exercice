var Treatment = require('../models/treatment');
var mongoose = require("mongoose");

class TreatmentService {

    static addTreatment(start, end, text, doctor) {
        return Treatment({
            start: start,
            end: end,
            text: text,
            doctor: new mongoose.Types.ObjectId(doctor)
        }).save();
    }

    static getTreatment() {
        return Treatment.find().populate({ path: 'doctor', model: 'Doctor' });
    }

    static exists(value) {
        return Treatment.exists({ _id: value });
    }

    static deleteTreatment(id) {
        return Treatment.deleteOne({ _id: new mongoose.Types.ObjectId(id) });
    }

    static stringToDate(dateStr) {
        var dateSpl = dateStr.split(".");

        if (dateSpl.length !== 3) throw new Error("Date invalide");

        var date = new Date(dateSpl[1] + "." + dateSpl[0] + "." + dateSpl[2]);

        if (isNaN(date.getTime())) throw new Error("Date invalide");

        return date;
    }

}

module.exports = TreatmentService;