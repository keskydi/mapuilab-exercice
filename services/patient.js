var Patient = require('../models/patient');
var mongoose = require("mongoose");

class PatientService {

    static getPatients() {
        return Patient.find();
    }

    static getPatientsWithTreatment() {
        return Patient.find({ 'treatment.1': { $exists: true } });
    }

    static addPatient(firstName, lastName, age, sex) {
        return Patient({
            firstName: firstName,
            lastName: lastName,
            age: age,
            sex: sex
        }).save();
    }

    static addTreatment(patientId, treatmentId) {
        return Patient.findByIdAndUpdate(patientId, { $addToSet: { treatments: treatmentId } }, {
            new: true
        });
    }

    static async exists(value) {
        let a=  await Patient.exists({ _id: value });
        console.log(a);
        if (!a)
            throw new Error("error input id patient");
        return true;
    }

    static updatePatient(id, firstName, lastName, age, sex) {
        return Patient.findByIdAndUpdate(new mongoose.Types.ObjectId(id), {
            firstName,
            lastName,
            age,
            sex
        }, {
            new: true
        });
    }

    static deletePatient(id) {
        //TODO : delete treatment link to the patient
        return Patient.deleteOne({ _id: new mongoose.Types.ObjectId(id) });
    }

    static getPatientById(id) {
        return Patient.findOne(new mongoose.Types.ObjectId(id)).populate([
            { path: 'drugs', model: 'Drug' },
            {
                path: 'treatments', model: 'Treatment', populate: {
                    path: 'doctor', model: 'Doctor'
                }
            }
        ]);
    }

}

module.exports = PatientService;