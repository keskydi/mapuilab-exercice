var Doctor = require('../models/doctor');
var Treatment =require('../models/treatment');

class DoctorService {

    static getDoctors() {
        return Doctor.find();
    }

    static addDoctor(firstName,lastName,speciality) {
        return Doctor({
            firstName: firstName,
            lastName: lastName,
            speciality: speciality
        }).save();
    }

    static exists(value) {
        return Doctor.exists({ _id: value });
    }

    static deleteDoctor(id) {
        return Doctor.deleteOne({ _id: new mongoose.Types.ObjectId(id) });
    }

    static getDoctorsWithTreatmentAndPatients() {
        return Doctor.aggregate([{
            $lookup: {
                from: "treatments",
                localField: "_id",
                foreignField: "doctor",
                as: "treatment"
            }
            }, {
                $unwind: {
                path: "$treatment",
                    preserveNullAndEmptyArrays: true
                }
            },{
            $lookup: {
                from: "patients",
                localField: "treatment._id",
                foreignField: "treatments",
                as: "treatment.patient"
            }
            }, {
                $group: {
                    _id: "$_id",
                    name: { $first: "$firstName" },
                    treatment: { $push: "$treatment" }
                }
            }]);
    }

}

module.exports = DoctorService;