// JavaScript source code
var mongoose = require('mongoose');

const {
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_HOSTNAME,
} = process.env;

mongoose.connect(`mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}/mapui?authSource=admin`, {
    useNewUrlParser: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 }
}).catch(function (error) {
    // An error has occured during first connection
    console.error(error);
    process.exit();
});

mongoose.set('debug', true);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));


db.once('open', function () {
    // we're connected!
    console.log("ok!!");
});

db.on('reconnected', function () {
    // reconnection
    console.log('MongoDB reconnected!');
});

db.on("disconnected", () => {
    console.log("Connection Disconnected");
});

db.on("close", () => {
    console.log("Connection Closed");
});

