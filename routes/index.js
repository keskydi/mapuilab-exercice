var express = require('express');
var router = express.Router();

var Doctor = require('../services/doctor');
var Treatment = require('../services/treatment');
var Patient = require('../services/patient');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


/**
 * @api {get} /populate add data to the database
 * @apiName Populate
 * @apiGroup Data
 *
 * @apiSuccess {String} message Success message
 *
  * @apiSuccessExample Success-Response:
  *     HTTP/1.1 200 OK
  *     {
            "message": "data added to database"
  *     }
 *
 * @apiErrorExample MongoError
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *      "error": {
            "name": "MongoError"
        }
 *     }
 *
 */

router.get('/populate',async function (req, res, next) {
    try {

        var d1 = await Doctor.addDoctor("antoine", "o", "spe1");
        var d2 = await Doctor.addDoctor("tomy", "o", "spe2");
        var d3 = await Doctor.addDoctor("john", "o", "spe3");

        var t1 = await Treatment.addTreatment(2002 - 12 - 09, 2003 - 12 - 09, "treat1", d1._id);
        var t2 = await Treatment.addTreatment(2002 - 12 - 09, 2003 - 12 - 09, "treat2", d1._id);
        var t3 = await Treatment.addTreatment(2002 - 12 - 09, 2003 - 12 - 09, "treat3", d2._id);
        var t4 = await Treatment.addTreatment(2002 - 12 - 09, 2003 - 12 - 09, "treat4", d2._id);
        var t5 = await Treatment.addTreatment(2002 - 12 - 09, 2003 - 12 - 09, "treat5", d3._id);
        var t6 = await Treatment.addTreatment(2002 - 12 - 09, 2003 - 12 - 09, "treat6", d3._id);

        var p1 = await Patient.addPatient("p1", "p1", 1, 18);
        var p2 = await Patient.addPatient("p2", "p3", 1, 18);
        var p3 = await Patient.addPatient("p3", "p3", 1, 18);

        p1 = await Patient.addTreatment(p1._id, t1._id);

        p2 = await Patient.addTreatment(p2._id, t5._id);
        p2 = await Patient.addTreatment(p2._id, t3._id);

        p3 = await Patient.addTreatment(p3._id, t2._id);
        p3 = await Patient.addTreatment(p3._id, t4._id);
        p3 = await Patient.addTreatment(p3._id, t6._id);

        res.json({
            message: "data add to database"
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });

    }
});

module.exports = router;
