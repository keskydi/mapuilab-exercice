var express = require('express');
var router = express.Router();

const Patient = require('../services/patient');

const { param, body, validationResult } = require('express-validator');
var ObjectId = require('mongoose').Types.ObjectId;


/**
 * @api {get} /patient/:id get a patient with his id
 * @apiName GetPatient
 * @apiGroup Patient
 * 
 * 
 * @apiParam (Param�tre URL) {String} id The id of the patient
 *
 * @apiSuccess {Object} patient The patient with the id in the url
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
                "data": {
                    "patient": {
                        "drugs": [],
                        "treatments": [
                            {
                                "_id": "5f2c8277ba1f3342f08a15b4",
                                "start": "1970-01-01T00:00:01.981Z",
                                "end": "1970-01-01T00:00:01.982Z",
                                "text": "treat1",
                                "doctor": {
                                    "_id": "5f2c8277ba1f3342f08a15b1",
                                    "firstName": "antoine",
                                    "lastName": "o",
                                    "speciality": "spe1",
                                    "__v": 0
                                },
                                "__v": 0
                            }
                        ],
                        "_id": "5f2c8277ba1f3342f08a15ba",
                        "firstName": "p1",
                        "lastName": "p1",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    }
                }
 *     }
 *
 * @apiErrorExample MongoError
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *      "error": {
            "name": "MongoError"
        }
 *     }
 *
 */
router.get('/:id', [
    param('id')
        .custom((value) => ObjectId.isValid(value))
        .withMessage("id invalid")
        .bail()
        .custom(async value => await Patient.exists(value)),
], async function (req, res) {
        try {

            const errors = validationResult(req);
            console.log(JSON.stringify(errors));
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

        let patient = await Patient.getPatientById(req.params.id);
        res.json({
            data: {
                patient: patient
            }
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });

    }
});




 /**
  * @api {put} /patient update a patient
  * @apiName UpdatePatient
  * @apiGroup Patient
  *
  *
  *
  * @apiParam { String } userId The id of the user
  * @apiParam { String } firstName The new firstname
  * @apiParam { String } lastName The new lastname
  * @apiParam { Number } age The new age
  * @apiParam { Number } sex the new sex
  *
  * @apiSuccess {Object} patient The updated patient
  *
  * @apiSuccessExample Success-Response:
  *     HTTP/1.1 200 OK
  *     {
                 "data": {
                     "patient": {
                         "drugs": [],
                         "treatments": [
                             {
                                 "_id": "5f2c8277ba1f3342f08a15b4",
                                 "start": "1970-01-01T00:00:01.981Z",
                                 "end": "1970-01-01T00:00:01.982Z",
                                 "text": "treat1",
                                 "doctor": {
                                     "_id": "5f2c8277ba1f3342f08a15b1",
                                     "firstName": "antoine",
                                     "lastName": "o",
                                     "speciality": "spe1",
                                     "__v": 0
                                 },
                                 "__v": 0
                             }
                         ],
                         "_id": "5f2c8277ba1f3342f08a15ba",
                         "firstName": "p1",
                         "lastName": "p1",
                         "age": 1,
                         "sex": 18,
                         "__v": 0
                     }
                 }
  *     }
  *
  * @apiErrorExample MongoError
  *     HTTP/1.1 500 Internal Server Error
  *     {
  *      "error": {
             "name": "MongoError"
         }
  *     }
  *
  */

router.put('/', [
    body("userId")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer l id de l utilisateur")
        .bail()
        .custom((value) => ObjectId.isValid(value))
        .withMessage("id invalid")
        .bail()
        .custom(async (value) => await Patient.exists(value))
        .withMessage("error input patient id treatment"),
    body("firstName")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer un prenom")
        .bail()
        .isLength({ max: 100 })
        .withMessage("Le texte de doit pas exceder 100 caracteres"),
    body("lastName")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer une description")
        .bail()
        .isLength({ max: 100 })
        .withMessage("Le texte de doit pas exceder 100 caracteres"),
    body("age")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer un age")
        .bail()
        .isNumeric()
        .withMessage("l age doit �tre un nombre"),
    body("sex")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer un sexe")
        .bail()
        .isIn([0, 1])
        .withMessage("doit etre 1 ou 0"),
], async function (req, res, next) {
        try {

            const errors = validationResult(req);
            console.log(JSON.stringify(errors));
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }
        await Patient.updatePatient(req.body.userId, req.body.firstName, req.body.lastName,req.body.age, req.body.sex);
        res.redirect('/patient/' + req.body.userId);
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });

    }
});


/**
  * @api {delete} /patient delete a patient and treatments associated
  * @apiName deletePatient
  * @apiGroup Patient
  *
  *
  * @apiParam { String } userId The id of the user
  *
  * @apiSuccess {Object} patient list of the patients
  * @apiSuccessExample Success-Response:
  *     HTTP/1.1 200 OK
  *     {
            "data": {
                "patients": [
                    {
                        "drugs": [],
                        "treatments": [
                            "5f2c8277ba1f3342f08a15b8",
                            "5f2c8277ba1f3342f08a15b6"
                        ],
                        "_id": "5f2c8277ba1f3342f08a15bb",
                        "firstName": "p2",
                        "lastName": "p3",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    },
                    {
                        "drugs": [],
                        "treatments": [
                            "5f2c8277ba1f3342f08a15b5",
                            "5f2c8277ba1f3342f08a15b7",
                            "5f2c8277ba1f3342f08a15b9"
                        ],
                        "_id": "5f2c8277ba1f3342f08a15bc",
                        "firstName": "p3",
                        "lastName": "p3",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    }
                ]
            }
  *     }
  *
  * @apiErrorExample MongoError
  *     HTTP/1.1 500 Internal Server Error
  *     {
  *      "error": {
             "name": "MongoError"
         }
  *     }
  *
  */

router.delete('/', [
    body("userId")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer l id de l utilisateur")
        .bail()
        .custom((value) => ObjectId.isValid(value))
        .withMessage("id invalid")
        .bail()
        .custom(async (value) => await Patient.exists(value))
        .withMessage("error input patient id treatment"),
], async function (req, res, next) {
    try {
        await Patient.deletePatient(req.body.userId);
        res.redirect('/patient');
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });

    }
});


/**
 * @api {post} /patient add a new patient
 * @apiName AddPatient
 * @apiGroup Patient
 *
 *
 * @apiParam { String } firstName The firstname
 * @apiParam { String } lastName The lastname
 * @apiParam { Number } age The age
 * @apiParam { Number } sex the sex
 *
 * @apiSuccess {Object} patient The list of patients
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
                "data": {
                    "patient": {
                        "drugs": [],
                        "treatments": [
                            {
                                "_id": "5f2c8277ba1f3342f08a15b4",
                                "start": "1970-01-01T00:00:01.981Z",
                                "end": "1970-01-01T00:00:01.982Z",
                                "text": "treat1",
                                "doctor": {
                                    "_id": "5f2c8277ba1f3342f08a15b1",
                                    "firstName": "antoine",
                                    "lastName": "o",
                                    "speciality": "spe1",
                                    "__v": 0
                                },
                                "__v": 0
                            }
                        ],
                        "_id": "5f2c8277ba1f3342f08a15ba",
                        "firstName": "p1",
                        "lastName": "p1",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    }
                }
 *     }
 *
 * @apiErrorExample MongoError
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *      "error": {
            "name": "MongoError"
        }
 *     }
 *
 */

router.post('/', [
    body("firstName")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer un prenom")
        .bail()
        .isLength({ max: 100 })
        .withMessage("Le texte de doit pas exceder 100 caracteres"),
    body("lastName")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer une description")
        .bail()
        .isLength({ max: 100 })
        .withMessage("Le texte de doit pas exceder 100 caracteres"),
    body("age")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer un age")
        .bail()
        .isNumeric()
        .withMessage("l age doit �tre un nombre"),
    body("sex")
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer un sexe")
        .bail()
        .isIn([0, 1])
        .withMessage("doit etre 1 ou 0"),
], async function (req, res, next) {
    try {
        await Patient.addPatient( req.body.firstName, req.body.lastName, req.body.age, req.body.sex);
        res.redirect('/patient');
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });

    }
});

/**
 * @api {get} /patient get list of patients
 * @apiName GetPatients
 * @apiGroup Patient
 *
 * @apiSuccess {Object} patient The list of patients
 *
  * @apiSuccessExample Success-Response:
  *     HTTP/1.1 200 OK
  *     {
            "data": {
                "patients": [
                    {
                        "drugs": [],
                        "treatments": [
                            "5f2c8277ba1f3342f08a15b8",
                            "5f2c8277ba1f3342f08a15b6"
                        ],
                        "_id": "5f2c8277ba1f3342f08a15bb",
                        "firstName": "p2",
                        "lastName": "p3",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    },
                    {
                        "drugs": [],
                        "treatments": [
                            "5f2c8277ba1f3342f08a15b5",
                            "5f2c8277ba1f3342f08a15b7",
                            "5f2c8277ba1f3342f08a15b9"
                        ],
                        "_id": "5f2c8277ba1f3342f08a15bc",
                        "firstName": "p3",
                        "lastName": "p3",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    }
                ]
            }
  *     }
 *
 * @apiErrorExample MongoError
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *      "error": {
            "name": "MongoError"
        }
 *     }
 *
 */

router.get('/', async function (req, res, next) {
    try {
        let patients = await Patient.getPatients();
        res.json({
            data: {
                patients: patients
            }
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });

    }
});

module.exports = router;
