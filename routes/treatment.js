var express = require('express');
var router = express.Router();

const Treatment = require('../services/treatment');
const Patient = require('../services/patient');
const Doctor = require('../services/doctor');

const { body, validationResult } = require('express-validator');

var ObjectId = require('mongoose').Types.ObjectId;

/**
 * @api {post} /treatment add a new treatment
 * @apiName AddTreatment
 * @apiGroup Treatment
 * 
 *
 * @apiParam { String } usereId The id of the user
 * @apiParam { String } doctorId The id of the doctor
 * @apiParam { Date } start the start of the treatment
 * @apiParam { Date } end the end of the treatment
 * @apiParam { String } text  description of the treatment
 *
 * @apiSuccess {Object} patient The detail of the patient
 *
  * @apiSuccessExample Success-Response:
  *     HTTP/1.1 200 OK
  *     {
            "data": {
                "patients": [
                    {
                        "drugs": [],
                        "treatments": [
                            "5f2c8277ba1f3342f08a15b8",
                            "5f2c8277ba1f3342f08a15b6"
                        ],
                        "_id": "5f2c8277ba1f3342f08a15bb",
                        "firstName": "p2",
                        "lastName": "p3",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    },
                    {
                        "drugs": [],
                        "treatments": [
                            "5f2c8277ba1f3342f08a15b5",
                            "5f2c8277ba1f3342f08a15b7",
                            "5f2c8277ba1f3342f08a15b9"
                        ],
                        "_id": "5f2c8277ba1f3342f08a15bc",
                        "firstName": "p3",
                        "lastName": "p3",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    }
                ]
            }
  *     }
 *
 * @apiErrorExample MongoError
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *      "error": {
            "name": "MongoError"
        }
 *     }
 *
 */
router.post('/', [
    body('userId')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer l id de l utilisateur")
        .bail()
        .custom( (value) => ObjectId.isValid(value))
        .withMessage("id invalid")
        .bail()
        .custom(async (value) => await Patient.exists(value))
        .withMessage("error input patient id treatment"),
    body('doctorId')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer l'id du doctor")
        .bail()
        .custom((value) => ObjectId.isValid(value))
        .withMessage("id invalid")
        .bail()
        .custom(async (value) => await Doctor.exists(value))
        .withMessage("error input doctor id treatment"),
    body('start')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer la date de depart")
        .bail()
        ,//.custom((value) =>Treatment.stringToDate(value)),
    body('end')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer la date de fin")
        .bail()
        ,//.custom((value) => Treatment.stringToDate(value)),
    body('text')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer une description")
        .isLength({ max: 256 })
        .withMessage("Le texte de doit pas exceder 256 caracteres"),
], async function (req, res) {
        try {

            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }


        await Patient.addTreatment(req.body.userId, await Treatment.addTreatment(req.body.start, req.body.end, req.body.text, req.body.doctorId));

        res.redirect('/patient/' + req.body.userId);
        
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });
    }
});


/**
 * @api {delete} /treatment delete a treatment
 * @apiName AddTreatment
 * @apiGroup Treatment
 *
 *
 * @apiParam { String } treatmentId The id of the treatment
 *
 * @apiSuccess {Object} doctor The list of doctor
 *
  * @apiSuccessExample Success-Response:
  *     HTTP/1.1 200 OK
  *     {
            "data": {
                "patients": [
                    {
                        "drugs": [],
                        "treatments": [
                            "5f2c8277ba1f3342f08a15b8",
                            "5f2c8277ba1f3342f08a15b6"
                        ],
                        "_id": "5f2c8277ba1f3342f08a15bb",
                        "firstName": "p2",
                        "lastName": "p3",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    },
                    {
                        "drugs": [],
                        "treatments": [
                            "5f2c8277ba1f3342f08a15b5",
                            "5f2c8277ba1f3342f08a15b7",
                            "5f2c8277ba1f3342f08a15b9"
                        ],
                        "_id": "5f2c8277ba1f3342f08a15bc",
                        "firstName": "p3",
                        "lastName": "p3",
                        "age": 1,
                        "sex": 18,
                        "__v": 0
                    }
                ]
            }
  *     }
 *
 * @apiErrorExample MongoError
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *      "error": {
            "name": "MongoError"
        }
 *     }
 *
 */
router.delete('/', [
    body('treatmentId')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer l'id du traitement")
        .bail()
        .custom((value) => ObjectId.isValid(value))
        .withMessage("id invalid")
        .bail()
        .custom(async (value) => await Treatment.exists(value))
        .withMessage("error input  id treatment"),
], async function (req, res) {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        await Treatment.deleteTreatment(req.body.treatmentId);
        res.redirect('/doctor');
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });

    }
});

/* GET users listing. */
router.get('/', function (req, res) {
    res.send('respond with a resource treatment');
});


module.exports = router;
