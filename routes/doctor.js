var express = require('express');
var router = express.Router();

const Doctor = require('../services/doctor');

/**
 * @api {get} /doctor get list of doctors
 * @apiName GetDoctor
 * @apiGroup Doctor
 *
 * @apiSuccess {Object} doctors The list of all the doctors
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
            "data": {
                "doctors": [
                    {
                        "_id": "5f2c761bfb13e764dc52b187",
                        "firstName": "john",
                        "lastName": "doe",
                        "speciality": "spe3",
                        "__v": 0
                    }
                ]
            }
 *     }
 *
 *
 * @apiErrorExample MongoError
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *      "error": {
            "name": "MongoError"
        }
 *     }
 *
 */
router.get('/', async function (req, res, next) {
    try {
        let doctors = await Doctor.getDoctors();
        res.json({
            data: {
                doctors: doctors
            }
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });

    }
});


/**
 * @api {get} /doctor/more get list of doctors with theire associated treatment and patient
 * @apiName GetDoctorMore
 * @apiGroup Doctor
 *
 * @apiSuccess {Object} doctors The list of all the doctors with theire associated treatment and patient
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
            "data": {
                "doctors": [
                    {
                        "_id": "5f2c8277ba1f3342f08a15b1",
                        "name": "antoine",
                        "treatment": [
                            {
                                "_id": "5f2c8277ba1f3342f08a15b4",
                                "start": "1970-01-01T00:00:01.981Z",
                                "end": "1970-01-01T00:00:01.982Z",
                                "text": "treat1",
                                "doctor": "5f2c8277ba1f3342f08a15b1",
                                "__v": 0,
                                "patient": [
                                    {
                                        "_id": "5f2c8277ba1f3342f08a15ba",
                                        "drugs": [],
                                        "treatments": [
                                            "5f2c8277ba1f3342f08a15b4"
                                        ],
                                        "firstName": "p1",
                                        "lastName": "p1",
                                        "age": 1,
                                        "sex": 18,
                                        "__v": 0
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
 *     }
 *
 * @apiError (Error 5xx) database_error The server could not add the note (maybe some of the params are malformed)
 *
 * @apiErrorExample MongoError
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *      "error": {
            "name": "MongoError"
        }
 *     }
 *
 */
router.get('/more', async function (req, res, next) {
    //res.send('respond with a resource patient');
    try {
        let doctors = await Doctor.getDoctorsWithTreatmentAndPatients();
        res.json({
            data: {
                doctors: doctors
            }
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: err
        });

    }
});

module.exports = router;
