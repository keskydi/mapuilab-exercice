var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var doctorRouter = require('./routes/doctor');
var drugRouter = require('./routes/drug');
var patientRouter = require('./routes/patient');
var treatmentRouter = require('./routes/treatment');

const mongoose = require('mongoose');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/doctor', doctorRouter);
app.use('/drug', drugRouter);
app.use('/patient', patientRouter);
app.use('/treatment', treatmentRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

var Doctor = require('./models/doctor');
var Treatment = require('./models/treatment');
var Patient = require('./models/patient');
var Drug = require('./models/drug');

require('./connect');


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', async function () {
    // we're connected!
    console.log("ok!! cc");

    /*
    await Doctor.createCollection();
    await Drug.createCollection();
    await Treatment.createCollection();
    await Patient.createCollection();
    */


});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
