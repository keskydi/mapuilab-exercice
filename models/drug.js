const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const drugShema = new Schema({
    name: { type: String, required: true },
    code: { type: String, required: true },
});

const Drug = mongoose.model('Drug', drugShema);
module.exports = Drug;