const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Treatment = require('./treatment');

const patientShema = new Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    age: { type: Number, required: true },
    sex: { type: Number, required: true },
    drugs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Drug' }],
    treatments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Treatment' }],
});



patientShema.pre('deleteOne', async function (next,model) {
    console.log("middleware deleteOne patient");
    const thisPatient = await this.model.findOne(this.getQuery());
    
    //console.log(JSON.stringify(thisPatient.treatments));

    mongoose.model("Treatment").deleteMany({ _id: { $in: thisPatient.treatments } }).exec();
    next();
});


const Patient = mongoose.model('Patient', patientShema);
module.exports = Patient;