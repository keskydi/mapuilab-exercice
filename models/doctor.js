const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const doctorShema = new Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    speciality: { type: String, required: true },
});


/*
doctorShema.pre('deleteOne', async function (next, model) {
    console.log("middleware deleteOne doctor");
    //const thisPatient = await this.model.findOne(this.getQuery());

    //console.log(JSON.stringify(thisPatient.treatments));

    mongoose.model("Treatment").deleteMany({ doctor: this.getQuery() }).exec();
    next();
});*/

const Doctor = mongoose.model('Doctor', doctorShema);
module.exports = Doctor;