const mongoose = require('mongoose');
//const 
const Schema = mongoose.Schema;

const treatmentShema = new Schema({
    start: { type: Date, required: true },
    end: { type: Date, required: true },
    text: { type: String, required: true },
    doctor: { type: mongoose.Schema.Types.ObjectId, ref:'Doctor'}
});

treatmentShema.pre('deleteOne', async function (next, model) {
    console.log("middleware deleteOne treat");
    const thisTreatment = await this.model.findOne(this.getQuery());

    console.log("test:" +JSON.stringify(thisTreatment));

    let count = await mongoose.model('Treatment').countDocuments({ doctor: thisTreatment.doctor });
    //console.log();

    if (count < 2) {
        await mongoose.model("Doctor").deleteOne({ _id: thisTreatment.doctor });
    }

    await mongoose.model("Patient").updateOne({ treatments: thisTreatment._id }, { $pull: { treatments: thisTreatment._id  }});
    //console.log(JSON.stringify(thisPatient.treatments));

    //mongoose.model("Treatment").deleteMany({ _id: { $in: thisPatient.treatments } }).exec();
    next();
});


const Treatment = mongoose.model('Treatment', treatmentShema);
module.exports = Treatment;