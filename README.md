# MaPui Labs exercice

## API Documentation
[API documentation is available here](https://keskydi.gitlab.io/mapuilab-exercice/)


## Dockerfile

### Building
```console
docker build -t mapuilab-exercice .
```

### Running

Run API:

```console
DB_URI="adresse mongo" npm start
```



